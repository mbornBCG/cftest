<cfcomponent name="CFtest" displayname="CFML testing library" hint="A stupidly simple testing library." description="CFTest: A one-file, simple Coldfusion testing library. See https://bitbucket.org/mbornBCG/cftest" output="true">

  <!--- Holds all test functions in an array of structs.  --->
  <cfset this.tests = ArrayNew() />

  <!--- holds the description of the current suite of tests. --->
  <cfset this.suitename = "" />

  <cffunction name="beginHTML" hint="Print opening of boilerplate HTML template for the test result page" output="true">
    <cfoutput>
      <!DOCTYPE html>
      <html>
        <head>
          <title>#this.suitename#</title>
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <cfset this.loadAssets() />
        </head>
        <body>
    </cfoutput>
  </cffunction>

  <cffunction name="endHTML" hint="Close up the HTML template" output="true">
    <cfoutput>

        </body>
      </html>
    </cfoutput>
  </cffunction>

  <cffunction name="loadAssets" hint="Stylesheets, scripts, etc. for nicer-looking test results." output="true">
    <cfoutput>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.1/css/bulma.min.css">
    </cfoutput>
  </cffunction>

  <cffunction name="describe" output="false" hint="Describe the test suite">
    <cfargument name="desc" required="true" type="string" />
    
    <cfset this.suitename = arguments.desc />

    <cfreturn true />
  </cffunction>

  <cffunction name="run" hint="Begins the entire thing!" description="Outputs a nice HTML page for the tests, then runs all the tests." output="true">
    <cfset this.beginHTML() />

    <div class="section">
      <div class="container is-widescreen">
        <h1 class="title is-1">#this.suitename#</h1>
        <cfset this.runAll() />
      </div><!--/container -->
    </div><!--/section -->
    <cfset this.endHTML() />
  </cffunction>

  <cffunction name="test" output="false" hint="Add the test to the test suite." description="Adds the test name and closure to `this.tests` in prep for running them.">
    <cfargument name="testname" required="true" type="string" />
    <cfargument name="testfunc" required="true" />
    
    <!--- add the test function to our list of test functions --->
    <cfset ArrayAppend(this.tests,{
      testname = arguments.testname,
      testfunc = arguments.testfunc
    }) />

  </cffunction>
  <cffunction name="printArg" hint="Handle the nice printing of random arguments in tests." output="false">
    <cfargument name="arg" required="true" />
    
    <cfif isStruct(arguments.arg) OR
	    	IsQuery(arguments.arg) OR
		    	isArray(arguments.arg)>
	    <!--- prevent errors when the argument is a struct. --->
	    <cfset arguments.arg = Serialize(arguments.arg) />
    </cfif>
    <cfreturn '<code>' & HTMLEditFormat(arguments.arg) & '</code>' />
  </cffunction>

  <!--- 
    Matching functions: isValid(), isEqual(), isNotEqual(), etc.
   --->
  <cffunction name="hasKey">
    <cfargument name="arg1" required="true" />
    <cfargument name="arg2" required="true" />

    <cfset var passed = StructKeyExists(arguments.arg1,arguments.arg2) />

    <cfif NOT passed>
        <cfoutput><p><span class="tag is-danger">Test FAIL</span> #this.printArg(arguments.arg2)# is not a property of #this.printArg(arguments.arg1)#</p></cfoutput>
      <cfelse>
        <cfoutput><p><span class="tag is-success">Test PASSED</span> #this.printArg(arguments.arg2)# does exist as a property of #this.printArg(arguments.arg1)#</p></cfoutput>
    </cfif>

    <cfreturn passed />
  </cffunction>
  <cffunction name="contains">
    <cfargument name="arg1" required="true" />
    <cfargument name="arg2" required="true" />

    <cfset var passed = arguments.arg1 CONTAINS arguments.arg2 />

    <cfif NOT passed>
        <cfoutput><p><span class="tag is-danger">Test FAIL</span> #this.printArg(arguments.arg1)# does not contain #this.printArg(arguments.arg2)#</p></cfoutput>
      <cfelse>
        <cfoutput><p><span class="tag is-success">Test PASSED</span> #this.printArg(arguments.arg1)# contains #this.printArg(arguments.arg2)#</p></cfoutput>
    </cfif>

    <cfreturn passed />
  </cffunction>
  <cffunction name="notContains">
    <cfargument name="arg1" required="true" />
    <cfargument name="arg2" required="true" />

    <cfset var passed = arguments.arg1 DOES NOT CONTAIN arguments.arg2 />

    <cfif NOT passed>
        <cfoutput><p><span class="tag is-danger">Test FAIL</span> #this.printArg(arguments.arg1)# contains #this.printArg(arguments.arg2)#</p></cfoutput>
      <cfelse>
        <cfoutput><p><span class="tag is-success">Test PASSED</span> #this.printArg(arguments.arg1)# does not contain #this.printArg(arguments.arg2)#</p></cfoutput>
    </cfif>

    <cfreturn passed />
  </cffunction>
  <cffunction name="isValid">
    <cfargument name="arg1" required="true" />
    <cfargument name="arg2" required="true" />

    <cfset var passed = isValid(arguments.arg1,arguments.arg2) />

    <cfif NOT passed>
        <cfoutput><p><span class="tag is-danger">Test FAIL</span> #this.printArg(arguments.arg2)# is not of type #this.printArg(arguments.arg1)#</p></cfoutput>
      <cfelse>
        <cfoutput><p><span class="tag is-success">Test PASSED</span> #this.printArg(arguments.arg2)# is of type #this.printArg(arguments.arg1)#</p></cfoutput>
    </cfif>

    <cfreturn passed />
  </cffunction>
  <cffunction name="isLessThan">
    <cfargument name="arg1" required="true" />
    <cfargument name="arg2" required="true" />

    <cfset var passed = arg1 LT arg2>

    <cfif NOT passed>
      <cfoutput><p><span class="tag is-danger">Test FAIL</span> #this.printArg(arguments.arg1)# is not less than #this.printArg(arguments.arg2)#</p></cfoutput>
    <cfelse>
      <cfoutput><p><span class="tag is-success">Test PASSED</span> #this.printArg(arguments.arg1)# is less than #this.printArg(arguments.arg2)#</p></cfoutput>
    </cfif>

    <cfreturn passed />
  </cffunction>
  <cffunction name="isGreaterThan">
    <cfargument name="arg1" required="true" />
    <cfargument name="arg2" required="true" />

    <cfset var passed = arg1 GT arg2>

    <cfif NOT passed>
      <cfoutput><p><span class="tag is-danger">Test FAIL</span> #this.printArg(arguments.arg1)# is not greater than #this.printArg(arguments.arg2)#</p></cfoutput>
    <cfelse>
      <cfoutput><p><span class="tag is-success">Test PASSED</span> #this.printArg(arguments.arg1)# is greater than #this.printArg(arguments.arg2)#</p></cfoutput>
    </cfif>

    <cfreturn passed />
  </cffunction>
  <cffunction name="isNotEqual">
    <cfargument name="arg1" required="true" />
    <cfargument name="arg2" required="true" />

    <cfset var passed = arg1 NEQ arg2>

    <cfif NOT passed>
      <cfoutput><p><span class="tag is-danger">Test FAIL</span> #this.printArg(arguments.arg1)# is equal to #this.printArg(arguments.arg2)#</p></cfoutput>
    <cfelse>
      <cfoutput><p><span class="tag is-success">Test PASSED</span> #this.printArg(arguments.arg1)# is not equal to #this.printArg(arguments.arg2)#</p></cfoutput>
    </cfif>

    <cfreturn passed />
  </cffunction>
  <cffunction name="isEqual">
    <cfargument name="arg1" required="true" />
    <cfargument name="arg2" required="true" />

    <cfset var passed = arg1 EQ arg2>

    <cfif NOT passed>
      <cfoutput><p><span class="tag is-danger">Test FAIL</span> #this.printArg(arguments.arg1)# is not equal to #this.printArg(arguments.arg2)#</p></cfoutput>
    <cfelse>
      <cfoutput><p><span class="tag is-success">Test PASSED</span> #this.printArg(arguments.arg1)# is equal to #this.printArg(arguments.arg2)#</p></cfoutput>
    </cfif>

    <cfreturn passed />
  </cffunction>
  <cffunction name="isFalse">
    <cfargument name="arg1" required="true" />

    <cfset var passed = NOT arg1 />

    <cfif NOT passed>
      <cfoutput><p><span class="tag is-danger">Test FAIL</span> #this.printArg(arguments.arg1)# is not false</p></cfoutput>
    <cfelse>
      <cfoutput><p><span class="tag is-success">Test PASSED</span> #this.printArg(arguments.arg1)# is false</p></cfoutput>
    </cfif>

    <cfreturn passed />
  </cffunction>
  <cffunction name="isTrue">
    <cfargument name="arg1" required="true" />

    <cfset var passed = arg1 />

    <cfif NOT passed>
      <cfoutput><p><span class="tag is-danger">Test FAIL</span> #this.printArg(arguments.arg1)# is not true</p></cfoutput>
    <cfelse>
      <cfoutput><p><span class="tag is-success">Test PASSED</span> #this.printArg(arguments.arg1)# is true</p></cfoutput>
    </cfif>

    <cfreturn passed />
  </cffunction>

  <!--- Can't figure this out yet, and in a hurry, so I'm pseudo-coding it for now. --->
  <cffunction name="runAll" hint="the runner function that tells runTest to invoke each function, one by one.">
    <cfset LOCAL.success = true />

    <cfloop from="1" to="#ArrayLen(this.tests)#" index="n">
      <cfset LOCAL.x = this.runTest(n) />
      <cfif NOT LOCAL.x><cfset LOCAL.success = false /></cfif>
    </cfloop>

    <cfreturn LOCAL.success />
  </cffunction>
  <cffunction name="runTest">
    <cfargument name="testnum" required="true" hint="The name of the user-defined function we wish to invoke" />
    <cfset LOCAL.success = false />
    
    <cfoutput>
      <cftry>
        <div class="test box" data-testname="#this.tests[arguments.testnum].testname#">
          <h2 class="title is-5">#this.tests[arguments.testnum].testname#</h2>
          <!--- Sadly, this doesn't work... yet.
          <pre class="textarea">
            #serialize(this.tests[arguments.testnum])#
          </pre>--->

          <!--- invoke the defined test method --->
          <cfset LOCAL.success = this.tests[arguments.testnum].testfunc() />
          
          <cfcatch type="any">
            <cfoutput>ERROR: #cfcatch.message#<br /><cfif StructKeyExists(cfcatch,"details")>#cfcatch.details#</cfif></cfoutput>
            <cfset LOCAL.success = false />
          </cfcatch>
        </div>
      </cftry>
    </cfoutput>

    <cfreturn LOCAL.success />
  </cffunction>
  
</cfcomponent>