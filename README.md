# CFTest
A Coldfusion testing library based on [Jasmine](https://jasmine.github.io/api/edge/).

## Installation and Setup
Seriously? It's a single file.

1. Copy `cftest.cfc` to your application.
2. Create `myTests.cfm`.
3. Follow Beginners Guide below for writing your first test.

## Beginners Guide
First, script syntax is recommended, but not really necessary.
```cfml
<cfscript>
...
</cfscript>
```

Next, include the `CFTest` framework.
```cfml
tester = CreateObject("cftest");
```

Include a description of this test "suite".
```cfml
tester.describe("Jesseborn.com website tests");
```

Begin your individual tests by describing them:
```cfml
tester.test("Site robots.txt exists",...);
```

Finally, add a "closure" function which houses the test logic.
```cfml
tester.test("Site robots.txt exists",function() {
    // CURL the website and see what it says.
    cfhttp(url="https://www.jesseborn.com/robots.txt", method="GET", result="theResponse");

    return tester.isEqual(theResponse.statusCode,"200 OK");
});
```

We can test as many items as we wish inside a single test - but your tests should remain as small as possible.
```cfml
return tester.isEqual(theResponse.statusCode,"200 OK") &&
        tester.contains(theResponse.filecontent,"Sitemap:");
```

## TODO
Wow, this is such a small little testing library. Where to go from here?

1. Nicer syntax, more like Jasmine. `expect(X).toBe(1.0)`
2. Better debugging, see 1. "Expected X to be 1.0, was 1.1"
3. View source code of test. (Turns out you can't Serialize() a closure very well.)
4. Mocking would be useful, but I feel would be better handled in a separate mini-library.