<cfscript>
  tester = CreateObject("cftest");

  tester.describe("Jesseborn.com website tests");

  tester.test("Un-secured homepage redirects to secured URL",function() {
    // CURL the website and see what it says.
    cfhttp(url="http://jesseborn.com/", method="GET", result="theResponse");

    return tester.isEqual(theResponse.statusCode,"301");
  });
  tester.test("Homepage secure URL returns 200 OK",function() {
    // CURL the website and see what it says.
    cfhttp(url="https://www.jesseborn.com/", method="GET", result="theResponse");

    return tester.isEqual(theResponse.statusCode,"200 OK");
  });
  tester.test("robots.txt exists",function() {
    // CURL the website and see what it says.
    cfhttp(url="https://www.jesseborn.com/robots.txt", method="GET", result="theResponse");

    return tester.isEqual(theResponse.statusCode,"200 OK") &&
            tester.contains(theResponse.filecontent,"Sitemap:");
  });
  tester.test("sitemap.xml exists",function() {
    // CURL the website and see what it says.
    cfhttp(url="https://www.jesseborn.com/sitemap.xml", method="GET", result="theResponse");

    return tester.isEqual(theResponse.statusCode,"200 OK") &&
            tester.contains(theResponse.filecontent,"<urlset");
  });

  // run the tests!
  tester.run();
</cfscript>